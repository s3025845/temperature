package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {
    public int getCelsius(String celsius){
        return (Integer.parseInt(celsius) * 9/5) +32;
    }
}
